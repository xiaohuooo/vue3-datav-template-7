# vuedatav

## Project setup

```
pnpm install
```

### Compiles and hot-reloads for development

```
pnpm run serve
```

### Compiles and minifies for production

```
pnpm run build
```

### Lints and fixes files

```
pnpm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

![alt text](image-1.png)
![alt text](xuqiu.png)
https://blog.csdn.net/JingDuo0909/article/details/129441974
